# tests #
The sketches in this directory are intended to be checkin tests.
**No code should be pushed to github without these tests passing.**

## Usage ##
See **runtests.sh** script inside each sketch dir. This script is fully compatible with **git _bisect_**.

### Prerequisites ###
In addition to the "standard" libraries, the tests require:
- python
- py-serial