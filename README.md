# RF24 #
A mature, optimized driver for the low-cost **NRF24L01_(+)_** _2.4 GHz wireless transceiver, with support for a multitude of platforms.

## Platforms ##
Generally, the supported platforms include embedded Linux systems, and Arduino-compatible microcontrollers and development platforms.

### Primary Platforms ###
The primary targeted platforms are:
- Arduino
- The _Raspberry Pi Family_:
  - Raspberry Pi (first generation)
  - Raspberry Pi 2
  - Raspberry Pi 3
  - Raspberry Pi Zero

### Additional Platforms ###
Additional platforms, which have at least _some degree_ of support, include the following:
- Some of the _ATTiny family_:
  - ATTiny85
  - ATTiny84
- ATXMega256D3
- [LittleWire](http://littlewire.cc/) devices
- [MRAA](http://c.mraa.io)-compatible devices
- [Teensy](http://www.pjrc.com/teensy/)
- Various other Linux-based embedded devices via **SPIDEV**


## Documentation ##
As the state of the cleanup project is not mature enough to generate documentation, please
refer to the documentation produced by _TMRh20_; this entire project has been forked from
his fork of the RF24 libraries.

- [Documentation Index][1]
- [RF24 Documentation][2]
- [Upstream Repository][3]


[1]: <http://tmrh20.github.io>
[2]: <http://tmrh20.github.io/RF24>
[3]: <https://github.com/TMRh20/RF24>
